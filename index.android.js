/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { Sentry } from 'react-native-sentry';
import thunk from 'redux-thunk';

import reducers from './src/reducers';

import App from './src/containers/App';

Sentry.config('https://9f94c5f58a7a44f5b92f919aaea3c900:27a3d6a81edc49e98f7ee9644ac2a6e0@sentry.io/1205700').install();

// const middleware = [thunk];
const store = createStore(
  reducers,
  applyMiddleware(thunk),
);

export default class Amozzi extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Provider store={store}>
        <App />
      </Provider>
    );
  }
}

AppRegistry.registerComponent('amozzi', () => Amozzi);
