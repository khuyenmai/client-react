import Setting from '../settings/setting.json';
import Header from './Header';

export const getCampaigns = async () => {
  const params = await Header.getRequestParams();
  return new Promise((resolve, reject) => {
    fetch(`${Setting.HOST}/api/v1.0.0/mobile/campaigns?`, params)
      .then(response => response.json())
      .then(resJson => resolve(resJson))
      .catch(error => reject(error));
  });
};

export const followCampaign = async (merchant, isFollowing) => {
  let params = await Header.getRequestParams();
  params = { ...params, method: 'POST', body: JSON.stringify({ merchant, isFollowing }) };
  return fetch(`${Setting.HOST}/api/v1.0.0/mobile/campaign/follow`, params)
    .then(response => response.json());
};

export const getFollowing = async () => {
  const params = await Header.getRequestParams();
  return fetch(`${Setting.HOST}/api/v1.0.0/mobile/following`, params)
    .then(response => response.json());
};
