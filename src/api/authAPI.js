
import FBSDK from 'react-native-fbsdk';
import Setting from '../settings/setting.json';

const {
  LoginManager,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} = FBSDK;

const authAPI = {
  accessFacebook: () => new Promise((resolve, reject) => {
    LoginManager.logInWithReadPermissions(['public_profile', 'email', 'user_friends'])
      .then(
        (result) => {
          if (result.isCancelled) return reject({ reason: 'cancel' });
          return AccessToken.getCurrentAccessToken();
        },
        () => reject({ reason: 'permission denied' }),
      )
      .then((data) => {
        if (!data || !data.accessToken) return reject({ reason: 'no accessToken' });
        const accessToken = data.accessToken;
        const infoRequest = new GraphRequest(
          '/me',
          {
            accessToken,
            parameters: {
              fields: {
                string: 'email,name,first_name,middle_name,last_name,picture',
              },
            },
          },
          (error, user) => {
            if (error) return reject({ reason: 'nouser' });
            return resolve({ user: { ...user, accessToken } });
          },
        );
        return new GraphRequestManager().addRequest(infoRequest).start();
      });
  }),
  login: user => new Promise((resolve, reject) => {
    fetch(`${Setting.HOST}/api/v1.0.0/mobile/login`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(user),
    })
      .then(response => response.json())
      .then((response) => {
        if (response.message === 'User Not Found') {
          return reject({ reason: 'Email chưa đăng ký' });
        } else if (response.message === 'Wrong password') {
          return reject({ reason: 'Email hoặc mật khẩu không đúng' });
        }
        return resolve(response);
      });
  }),
};

export default authAPI;
