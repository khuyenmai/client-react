import Setting from '../settings/setting.json';
import Header from './Header';

export const getTopProducts = async (option = {}) => {
  const params = await Header.getRequestParams();
  return new Promise((resolve, reject) => {
    const url = `${Setting.HOST}/api/v1.0.0/mobile/top-products?limit=${option.limit || 20}&skip=${option.skip || 0}&fields=${option.fildes || ''}`;
    fetch(`${url}`, params)
      .then(response => response.json())
      .then(resJson => resolve(resJson))
      .catch(error => reject(error));
  });
};

export const searchProducts = async (searchText) => {
  const params = await Header.getRequestParams();
  return new Promise((resolve, reject) => {
    const url = `${Setting.HOST}/api/v1.0.0/mobile/search-products?searchText=${searchText}`;
    fetch(`${url}`, params)
      .then(response => response.json())
      .then(resJson => resolve(resJson))
      .catch(error => reject(error));
  });
};
