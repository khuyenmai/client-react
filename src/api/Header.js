import { AsyncStorage } from 'react-native';

const getToken = async () => {
  try {
    const user = await AsyncStorage.getItem('user') || '{}';
    const { token = '' } = JSON.parse(user);
    return token;
  } catch (error) {
    return '';
  }
};

class Header {
  getRequestParams = async () => {
    const token = await getToken();
    return {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Token ${token}`,
      },
    };
  }
}

export default new Header();
