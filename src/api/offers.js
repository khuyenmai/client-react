import Setting from '../settings/setting.json';
import Header from './Header';

export const getOffers = async (option = {}) => {
  const params = await Header.getRequestParams();
  const url = `${Setting.HOST}/api/v1.0.0/mobile/offers?`;
  return new Promise((resolve, reject) => {
    fetch(`${url}limit=${option.limit || 40}&skip=${option.skip || 0}&fields=${option.fildes || ''}`,
      params)
      .then(response => response.json())
      .then(resJson => resolve(resJson))
      .catch(error => reject(error));
  });
};

export const like = async (kind, id) => {
  const params = await Header.getRequestParams();
  return new Promise((resolve, reject) => {
    fetch(`${Setting.HOST}/api/v1.0.0/mobile/${kind}/${id}/like`, {
      ...params,
      method: 'GET',
    })
      .then(response => response.json())
      .then(resJson => resolve(resJson))
      .catch(error => reject(error));
  });
};

export const createComment = async (target, id, comment) => {
  const params = await Header.getRequestParams();
  return new Promise((resolve, reject) => {
    fetch(`${Setting.HOST}/api/v1.0.0/mobile/${target}/${id}/comment`, {
      ...params,
      method: 'POST',
      body: { comment },
    })
      .then(response => response.json())
      .then(resJson => resolve(resJson))
      .catch(error => reject(error));
  });
};
