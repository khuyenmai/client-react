import { getOffers, like } from '../api/offers';
import { getTopProducts, searchProducts } from '../api/products';
import { TOGGLE_MENU, FETCHING_DONE, UPDATE_CURRENT_NAVIGATOR } from './constants';

export const actionGetOffers = dispatch => getOffers({})
  .then(({ data }) => {
    dispatch({
      type: FETCHING_DONE,
      offers: data,
    });
  });

export const actionGetTopProducts = dispatch => getTopProducts({})
  .then(({ data }) => Promise.all(data.map(({ image }) => fetch(image)))
    .then((results) => {
      const fails = results.filter(({ ok }) => !ok).map(({ url }) => url);
      return data.filter(({ image }) => !fails.includes(image));
    })).then((data) => {
    if (!data) return;
    dispatch({
      type: FETCHING_DONE,
      products: data,
    });
  });

export const toggleMenu = dispatch => dispatch({
  type: TOGGLE_MENU,
});

export const updateCurrentNavigator = (dispatch, currentnavigation) => dispatch({
  type: UPDATE_CURRENT_NAVIGATOR,
  currentnavigation,
});

export const actionSearchProducts = searchText => dispatch => searchProducts(searchText)
  .then(({ data }) => {
    if (!data) return;
    dispatch({
      type: FETCHING_DONE,
      products: data,
    });
  });

export const actionLike = (kind, id) => like(kind, id)
  .then(() => {});
