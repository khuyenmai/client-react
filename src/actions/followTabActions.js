import { getCampaigns, getFollowing } from '../api/campaigns';
import { GET_CAMPAINS_DONE, GET_FOLLWING_DONE } from './constants';

export const actionGetCampaigns = dispatch => getCampaigns()
  .then(({ data }) => {
    dispatch({
      type: GET_CAMPAINS_DONE,
      payload: data,
    });
  })
  .catch(() => {
    dispatch({
      type: GET_CAMPAINS_DONE,
      payload: [],
    });
  });

export const actionGetFollowing = dispatch => getFollowing().then(({ data }) => {
  dispatch({
    type: GET_FOLLWING_DONE,
    payload: data,
  });
})
  .catch(() => {
    dispatch({
      type: GET_FOLLWING_DONE,
      payload: [],
    });
  });
