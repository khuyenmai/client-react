export const APP_NAME = 'AMOZZI';

export const FETCHING_DONE = `${APP_NAME}_FETCHING_DONE`;
export const UPDATE_CURRENT_NAVIGATOR = `${APP_NAME}_UPDATE_CURRENT_NAVIGATOR`;
export const ORDER_DETAIL = `${APP_NAME}_ORDER_DETAIL`;
export const TOGGLE_MENU = `${APP_NAME}_TOGGLE_MENU`;
export const GET_CAMPAINS_DONE = `${APP_NAME}_GET_CAMPAINS_DONE`;
export const GET_FOLLWING_DONE = `${APP_NAME}_GET_FOLLWING_DONE`;
