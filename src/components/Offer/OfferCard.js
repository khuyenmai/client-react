import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import { connect } from 'react-redux';
import {
  Image, TouchableOpacity, Dimensions, View,
} from 'react-native';
import {
  Card, CardItem, Left, Thumbnail, Body, Text, Icon, Right, Badge,
} from 'native-base';
import { merchants, defaultLogo } from '../../settings/list_campaigns.json';

import { followCampaign } from '../../api/campaigns';

const screenWidth = Dimensions.get('window').width;

function capitalizeString(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}
class OfferCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      offer: props.offer,
      followingMerchants: props.followingMerchants,
      imageSize: {
        width: 0,
        height: 0,
      },
      isClick: false,
    };
  }

  componentWillMount() {
    this.scaleImage();
  }

  onClickFollow = (offer, isFollowing) => {
    const { followingMerchants } = this.state;
    followCampaign(offer.merchant, isFollowing);
    if (isFollowing) {
      const index = followingMerchants.indexOf(offer.merchant);
      followingMerchants.splice(index, 1);
    } else {
      followingMerchants.push(offer.merchant);
    }
    this.setState({ followingMerchants });
  }

  getMerchant = () => {
    const { offer } = this.state;
    const offerMerchant = (merchants || []).find((strMerchant) => {
      const merchant = JSON.parse(strMerchant);
      return offer.domain === merchant.website;
    });
    return offerMerchant ? JSON.parse(offerMerchant) : {};
  }

  handlePress = (offer) => {
    const { isClick } = this.state;
    const { navigation } = this.props;
    if (isClick || !navigation) return false;
    return this.setState({ isClick: true }, () => {
      navigation.navigate('OfferDetail', {
        ...offer, logo: this.getMerchant().website_logo || defaultLogo,
      });
      setTimeout(() => (this.setState({ isClick: false })), 1000);
    });
  };

  handleLike = () => {
    const { offer, offer: { likes } } = this.state;
    if (offer.isLiked) {
      likes.pop();
    } else {
      likes.push({});
    }
    this.props.actionLike('offer', offer._id);
    this.setState({
      offer: {
        ...offer,
        likes,
        isLiked: !offer.isLiked,
      },
    });
  }

  handleComment = () => {

  }

  scaleImage = () => {
    const { offer } = this.state;
    Image.getSize(offer.image, (width, height) => {
      const ratio = (screenWidth - 10) / width;
      let imageHeight = (Math.round((height * ratio) / 10) * 10) + 8;
      // const rank = [88, 98, 108, 118, 128, 138, 148, 158 ];
      if (imageHeight > 198) imageHeight = 307;

      this.setState({
        imageSize: {
          height: imageHeight,
        },
      });
    });
  }

  render() {
    const { offer, imageSize, followingMerchants } = this.state;
    // const { followingMerchants } = this.props;
    // console.log('offer', offer)
    return (
      <Card>
        <CardItem>
          <Left>
            <Thumbnail
              source={{ uri: this.getMerchant().website_logo || defaultLogo }}
              style={{ height: 40, width: 40 }}
            />
            <Body>
              <Text>
                {capitalizeString(offer.merchant.split('.')[0])}
              </Text>
              <Text note>
                {offer.domain}
              </Text>
            </Body>
            <Right>
              {
                followingMerchants.includes(offer.merchant) ? (
                  <TouchableOpacity onPress={() => this.onClickFollow(offer, true)}>
                    <Badge style={{ backgroundColor: '#2abe87' }}>
                      <Icon name="md-star-outline" style={{ fontSize: 19, color: '#fff', lineHeight: 25 }} />
                    </Badge>
                  </TouchableOpacity>
                ) : (
                  <TouchableOpacity onPress={() => this.onClickFollow(offer, false)}>
                    <Badge style={{ backgroundColor: '#f4f5fc', flexDirection: 'row' }}>
                      <Icon name="md-star" style={{ fontSize: 19, color: '#2abe87', lineHeight: 25 }} />
                      <Text style={{ color: '#2abe87', fontSize: 13, lineHeight: 30 }}> Theo dõi </Text>
                    </Badge>
                  </TouchableOpacity>
                )
              }
            </Right>
          </Left>
        </CardItem>
        <CardItem cardBody>
          <TouchableOpacity
            onPress={() => this.handlePress(offer)}
            style={{ width: '100%' }}
          >
            <Image
              source={{ uri: offer.image || '' }}
              style={{ height: imageSize.height, width: '100%', flex: 1 }}
            />
          </TouchableOpacity>
        </CardItem>
        <CardItem>
          <View style={{ flex: 1 }}>
            <TouchableOpacity
              transparent
              onPress={() => this.handleLike(offer._id)}
              style={{ height: 20, width: 30 }}
            >
              <Icon style={{ fontSize: 16, color: offer.isLiked ? '#FF507A' : undefined }} name={offer.isLiked ? 'ios-heart' : 'ios-heart-outline'}>
                <Text> {offer.likes.length || 0} </Text>
              </Icon>
            </TouchableOpacity>
          </View>
          <View style={{ flex: 1 }}>
            <TouchableOpacity>
              <Icon style={{ fontSize: 16 }} name="ios-chatbubbles-outline">
                <Text> 4 </Text>
              </Icon>
            </TouchableOpacity>
          </View>
          <View style={{ flex: 1 }}>
            <Icon style={{ fontSize: 16, width: 150 }} name="ios-timer-outline">
              <Text>
                {' '}
                {moment(offer.end_time).format('DD/MM/YYYY')}
              </Text>
            </Icon>
          </View>
        </CardItem>
      </Card>
    );
  }
}

OfferCard.propTypes = {
  offer: PropTypes.object,
  // dispatch: PropTypes.func,
  actionLike: PropTypes.any,
  navigation: PropTypes.object,
  followingMerchants: PropTypes.array,
};

const mapStateToProps = ({ homeReducer }) => ({
  offerDetail: homeReducer.offerDetail,
  navigation: homeReducer.currentnavigation,
});

export default connect(mapStateToProps)(OfferCard);
