import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropType from 'prop-types';
import { StyleSheet, TouchableOpacity } from 'react-native';
import {
  Header,
  Left,
  Body,
  Right,
} from 'native-base';

import Ionicons from 'react-native-vector-icons/Ionicons';

import { toggleMenu } from '../../actions/salesTabActions';

const styles = StyleSheet.create({
  tabHeading: {
    backgroundColor: '#FFF',
  },
  headerLeft: {
    flex: 1 / 2,
  },
  headerRight: {
    flex: 1 / 2,
  },
  headerBody: {
    flex: 1,
    alignItems: 'center',
  },
  header: {
    backgroundColor: '#2abe87',
  },
});

class HeaderBar extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { dispatch } = this.props;
    return (
      <Header style={styles.header}>
        <Left style={styles.headerLeft}>
          <TouchableOpacity onPress={() => toggleMenu(dispatch)}>
            <Ionicons name="ios-options-outline" style={{ fontSize: 30, color: '#FFF' }} />
          </TouchableOpacity>
        </Left>
        <Body style={styles.headerBody}>
          {/* <Title> Đăng ký </Title> */}
        </Body>
        <Right style={styles.headerRight}>
          <TouchableOpacity>
            <Ionicons name="ios-search-outline" style={{ fontSize: 30, color: '#FFF' }} />
          </TouchableOpacity>
        </Right>
      </Header>
    );
  }
}

HeaderBar.propTypes = {
  dispatch: PropType.any,
};

const mapStateToProps = () => ({});

export default connect(mapStateToProps)(HeaderBar);
