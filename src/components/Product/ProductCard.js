
import React, { Component } from 'react';
import { Image, View, TouchableOpacity } from 'react-native';
import {
  Left,
  Card,
  Text,
  CardItem,
  Thumbnail,
} from 'native-base';

import { merchants } from '../../settings/list_campaigns.json';

class ProductCard extends Component {
  getMerchant = () => {
    const { product } = this.props;
    const productMerchant = (merchants || []).find((strMerchant) => {
      const merchant = JSON.parse(strMerchant);
      return product.domain === merchant.website;
    });
    return productMerchant ? JSON.parse(productMerchant) : {};
  };

  handlePress = (product) => {
    const merchant = this.getMerchant();
    // const { isClick } = this.state;
    // if (isClick) return false;
    // this.setState({ isClick: true }, () => {
    this.props.openDetail({ ...product, logo: merchant.website_logo });
    // setTimeout(() => (this.setState({ isClick: false })), 1000);
    // })
  };

  render() {
    const { product } = this.props;
    const merchant = this.getMerchant();
    // console.log('merchant', merchant)
    return (
      <Card style={{ width: '50%' }}>
        <CardItem>
          <Left>
            {
              !merchant.website_logo ? (
                <Thumbnail
                  source={require('../../images/tag.png')}
                  style={{ height: 32, width: 32 }}
                />
              ) : (
                <Thumbnail
                  source={{ uri: merchant.website_logo }}
                  style={{ height: 32, width: 32 }}
                />
              )
            }

          </Left>
          <View>
            <Text style={{ fontSize: 12 }}>
              { `${String(product.price).replace(/\B(?=(\d{3})+(?!\d))/g, '.')}đ` }
            </Text>
          </View>
        </CardItem>
        <CardItem cardBody>
          <TouchableOpacity
            onPress={() => this.handlePress(product)}
            style={{ width: '100%', height: 220 }}
          >
            <Image source={{ uri: product.image }} style={{ height: 220, width: '96%' }} />
          </TouchableOpacity>
        </CardItem>
        <CardItem>
          <Text style={{ fontSize: 13 }}>
            {(product.name || '').substring(0, 60)}
            {product.name.length > 60 ? '....' : '' }
          </Text>
        </CardItem>
      </Card>
    );
  }
}

export default ProductCard;
