import React from 'react';
import { StackNavigator } from 'react-navigation';

import Intro from './intro';
import Home from './home/home';
import Category from './category/category';
import OfferDetail from './detail/OfferDetail';
import ProductDetail from './detail/ProductDetail';

const RootStack = StackNavigator({
  Intro: {
    screen: Intro,
    navigationOptions: {
      header: null,
    },
  },
  Home: {
    screen: Home,
    navigationOptions: {
      header: null,
    },
  },
  OfferDetail: {
    screen: OfferDetail,
    navigationOptions: {
      header: null,
    },
  },
  ProductDetail: {
    screen: ProductDetail,
    navigationOptions: {
      header: null,
    },
  },
  Category: {
    screen: Category,
    navigationOptions: {
      header: null,
    },
  },
},
{
  initialRouteName: 'Home',
});

const App = () => <RootStack />;
export default App;
