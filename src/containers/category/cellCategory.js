import React, { Component } from 'react';
import {
  Image, StyleSheet, Dimensions, Text,
} from 'react-native';

import { Col } from 'native-base';

const screenWidth = Dimensions.get('window').width;

// import Icon from "react-native-fa-icons";

const styles = StyleSheet.create({
  categoryImage: {
    width: screenWidth / 3,
    height: screenWidth / 3,
  },
});

export default class CellCategory extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Col style={styles.categoryImage}>
        <Image style={styles.categoryImage}>
          <Text>
            {this.props.category.name}
          </Text>
        </Image>
      </Col>
    );
  }
}
