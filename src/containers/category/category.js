import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, Dimensions } from 'react-native';

import {
  Container,
  Content,
  Header,
  Body,
  Right,
  Title,
  Left,
  Grid,
  Row,
  Thumbnail,
  Input,
  Item,
  Col,
} from 'native-base';

import Icon from 'react-native-vector-icons/Ionicons';
import CellCategory from './cellCategory';

const screenWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#2abe87',
  },
  headerBody: {
    alignItems: 'center',
  },
  categoryImage: {
    width: screenWidth / 3,
    height: screenWidth / 3,
  },
  itemSearch: {
    // borderWidth: 1,
    // borderColor: "#000",
    marginTop: 10,
    marginBottom: 10,
  },
  content: {
    backgroundColor: '#FFF',
  },
  gridCategory: {
    // marginTop: 10,
  },
});

export default class Category extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  renderCategories(categories) {
    const arrayRender = [];
    for (let i = 0; i < categories.length; i += 1) {
      const category = categories[i];
      arrayRender.push(<CellCategory category={category} />);
    }
    return arrayRender;
  }

  render() {
    const { navigate } = this.props.navigation;
    const categories = [
      { name: 'Category 1', image: '../../images/demo.jpg' },
      { name: 'Category 2', image: '../../images/demo.jpg' },
      { name: 'Category 3', image: '../../images/demo.jpg' },
    ];
    return (
      <Container behavior="padding" style={styles.container}>
        <Header style={styles.header}>
          <Left>
            <TouchableOpacity onPress={() => navigate('Home', {})}>
              <Icon name="ios-arrow-round-back" style={{ fontSize: 45, color: '#FFF' }} />
            </TouchableOpacity>
          </Left>
          <Body style={styles.headerBody}>
            <Title>
              {' '}
Chọn Danh mục
            </Title>
          </Body>
          <Right />
        </Header>
        <Content style={styles.content}>
          <Item rounded style={styles.itemSearch}>
            <Input placeholder="Search" />
          </Item>
          <Grid style={styles.gridCategory}>
            <Row style={{ backgroundColor: 'yellow' }}>
              {this.renderCategories(categories)}
            </Row>
            <Row style={{ backgroundColor: 'yellow' }}>
              <Col>
                <Thumbnail
                  style={styles.categoryImage}
                  square
                  source={require('../../images/demo.jpg')}
                />
              </Col>
            </Row>
            <Row style={{ backgroundColor: 'yellow' }}>
              <Col>
                <Thumbnail
                  style={styles.categoryImage}
                  square
                  source={{ uri: '../../images/logo.png' }}
                />
              </Col>
            </Row>
          </Grid>
        </Content>
      </Container>
    );
  }
}
