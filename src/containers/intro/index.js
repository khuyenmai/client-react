import React, { Component } from 'react';
import {
  Text, ImageBackground, Image, View, StyleSheet, Dimensions,
} from 'react-native';
import { Button } from 'native-base';

const logoWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backgroundContainer: {
    flex: 1,
    width: '100%',
  },
  logoContainer: {
    alignItems: 'center',
    flexGrow: 1,
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(43, 179, 127, 0.9)',
    paddingTop: 200,
  },
  logoView: {
    width: (logoWidth / 3) + 50,
    height: (logoWidth / 3) + 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 100,
    // backgroundColor: "rgba(11, 63, 44, 0.3)",
  },
  logo: {
    width: logoWidth / 3,
    height: logoWidth / 3,
  },
  appName: {
    fontWeight: '600',
    fontSize: 27,
    letterSpacing: -0.5,
    marginTop: 20,
    color: 'white',
  },
  btnView: {
    display: 'none',
    flexDirection: 'row',
    marginTop: logoWidth / 3,
  },
  btnSignView: {
    flex: 1 / 2,
    alignItems: 'center',
    padding: 10,
  },
  btnSign: {
    alignItems: 'center',
  },
  btnSignText: {
    fontWeight: '300',
    fontSize: 17,
    alignItems: 'center',
    color: 'white',
  },
});
export default class Intro extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground
          source={require('../../images/backgroundIntro.jpg')}
          style={styles.backgroundContainer}
          resizeMode="cover"
          blurRadius={2}
        >
          <View style={styles.logoContainer}>
            <View style={styles.logoView}>
              <Image style={styles.logo} source={require('../../images/logo.png')} />
            </View>
            <Text style={styles.appName}>
              {' '}
              AMOZZI
              {' '}
            </Text>
            <View style={styles.btnView}>
              <View style={styles.btnSignView}>
                <Button
                  large
                  block
                  rounded
                  success
                  style={styles.btnSign}
                >
                  <Text style={styles.btnSignText}>
Đăng ký
                  </Text>
                </Button>
              </View>
              <View style={styles.btnSignView}>
                <Button
                  large
                  block
                  rounded
                  success
                  style={styles.btnSign}
                >
                  <Text style={styles.btnSignText}>
Đăng nhập
                  </Text>
                </Button>
              </View>
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}
