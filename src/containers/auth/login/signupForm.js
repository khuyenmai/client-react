import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { Button, Input, Item } from 'native-base';
import Icon from 'react-native-vector-icons/EvilIcons';
// KeyboardAvoidingView
const styles = StyleSheet.create({
  buttonLogin: {
    height: 50,
    marginTop: 20,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 50,
    backgroundColor: '#2abe87',
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    textAlign: 'center',
  },
  itemInput: {
    marginLeft: 20,
    marginRight: 20,
  },
  input: {
    height: 40,
    marginTop: 10,
    marginBottom: 10,
    paddingHorizontal: 10,
    backgroundColor: 'rgba(255,255,255, 0.3)',
  },
  container: {
    padding: 20,
  },
});

export default class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      fullName: '',
      password: '',
      confirmPassword: '',
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Item underline style={styles.itemInput}>
          <Icon name="user" size={30} />
          <Input
            autoCorrect={false}
            returnKeyType="next"
            style={styles.input}
            autoCapitalize="none"
            placeholder="Họ và tên"
            value={this.state.fullName}
            onSubmitEditing={() => this.emailInput._root.focus()}
            onChangeText={fullName => this.setState({ fullName })}
          />
        </Item>
        <Item underline style={styles.itemInput}>
          <Icon name="envelope" size={30} />
          <Input
            placeholder="Email"
            autoCorrect={false}
            returnKeyType="next"
            style={styles.input}
            autoCapitalize="none"
            value={this.state.email}
            keyboardType="email-address"
            ref={input => (this.emailInput = input)}
            onChangeText={email => this.setState({ email })}
            onSubmitEditing={() => this.passwordInput._root.focus()}
          />
        </Item>
        <Item underline style={styles.itemInput}>
          <Icon name="unlock" size={30} />
          <Input
            secureTextEntry
            style={styles.input}
            autoCapitalize="none"
            placeholder="Mật khẩu"
            value={this.state.password}
            ref={input => (this.passwordInput = input)}
            onChangeText={password => this.setState({ password })}
            onSubmitEditing={() => this.confirmPasswordInput._root.focus()}
          />
        </Item>
        <Item underline style={styles.itemInput}>
          <Icon name="unlock" size={30} />
          <Input
            secureTextEntry
            style={styles.input}
            autoCapitalize="none"
            placeholder="Nhập lại mật khẩu"
            value={this.state.confirmPassword}
            ref={input => (this.confirmPasswordInput = input)}
            onChangeText={confirmPassword => this.setState({ confirmPassword })}
          />
        </Item>
        <Button
          onPress={() => this.props.submitSignup(this.state)}
          block
          style={styles.buttonLogin}
        >
          <Text style={styles.buttonText}>
            {' '}
            Đăng ký
            {' '}
          </Text>
        </Button>
        <Button transparent block primary onPress={this.props.changeForm}>
          <Text style={{ color: '#0000cd', fontSize: 18 }}>
            {' '}
            Đăng nhập
            {' '}
          </Text>
        </Button>
      </View>
    );
  }
}
