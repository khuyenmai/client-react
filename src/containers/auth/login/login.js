import React, { Component } from 'react';
import {
  Text, View, KeyboardAvoidingView, StyleSheet, Alert, Keyboard, AsyncStorage,
} from 'react-native';
import PropTypes from 'prop-types';
import { Button, Icon } from 'native-base';
import LoginForm from './loginForm';
import SignupForm from './signupForm';
import Setting from '../../../settings/setting.json';
import Loading from '../../../components/Loading/Loading';
import authAPI from '../../../api/authAPI';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  logoContainer: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center',
  },
  logo: {
    width: 100,
    height: 100,
  },
  appTitle: {
    fontWeight: '500',
    fontSize: 30,
  },
});

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = { loginForm: true, isLoading: false };
  }

  onLoginFacebook = () => {
    authAPI.accessFacebook()
      .then((user) => {
        const dbUser = {
          fullName: user.name,
          email: user.email,
          service: {
            provider: 'Facebook',
            user,
          },
          password: '',
        };
        this.handleAPILogin(dbUser);
      })
      .catch(() => Alert.alert('Đăng nhập không thành công'));
  }

  changeForm = () => {
    this.setState({
      loginForm: !this.state.loginForm,
    });
  }

  handleAPILogin = (user) => {
    const { loginSuccess } = this.props;
    return authAPI.login(user)
      .then(async (response) => {
        this.setState({ isLoading: false });
        await AsyncStorage.setItem('user', JSON.stringify(response));
        return Alert.alert(
          'Đăng nhập',
          'Thành công',
          [{ text: 'Ok', onPress: loginSuccess }, { cancelable: false }],
        );
      })
      .catch((error) => {
        this.setState({ isLoading: false });
        Alert.alert((error && error.reason) || 'Mất kết nối ...');
      });
  }

  submitLogin = (user) => {
    let message = '';
    if (!user.email || !user.password) message = 'Vui lòng điền đầy đủ thông tin';
    if (message) return Alert.alert(message);

    Keyboard.dismiss();
    this.setState({ isLoading: true });
    return this.handleAPILogin(user);
  }

  submitSignup = (user) => {
    let message = '';
    if (
      !user.email
      || !user.fullName
      || !user.password
      || !user.confirmPassword
    ) message = 'Vui lòng nhập đầy đủ thông tin';
    if (user.password !== user.confirmPassword) message = 'Sai mật khẩu';
    if (!this.validateEmail(user.email)) message = 'Email không hợp lệ';
    if (message) return Alert.alert(message);
    this.setState({ isLoading: true });

    return fetch(`${Setting.HOST}/api/v1.0.0/mobile/register`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(user),
    })
      .then((response) => {
        this.setState({ isLoading: false });
        if (response.status === 200) {
          // Success //
          response.json().then(async (res) => {
            await AsyncStorage.setItem('user', JSON.stringify(res));
            Alert.alert('Đăng ký', 'Thành công', [{
              text: 'Ok', onPress: () => this.props.loginSuccess(),
            }, { cancelable: false }]);
          });
        } else if (response.status === 400) {
          response.json().then((res) => {
            if (res.message.code === 11000) {
              Alert.alert('Email đã được đăng ký');
            } else {
              Alert.alert('Email hay password không đúng');
            }
          });
        }
      })
      .catch(() => {
        this.setState({ isLoading: false });
        Alert.alert('Mất kết nối ...');
      });
  }

  validateEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  render() {
    const { loginForm, isLoading } = this.state;
    return (
      <KeyboardAvoidingView behavior="padding" style={styles.container}>
        <View style={styles.logoContainer}>
          <Text style={styles.appTitle}>
            AMOZZI
          </Text>
          <View style={{ marginTop: 30 }}>
            <Button iconLeft light style={{ backgroundColor: '#3B5998' }} onPress={() => this.onLoginFacebook()} >
              <Icon style={{ color: '#fff' }} name="logo-facebook" />
              <Text style={{ color: '#fff' }} >{'  Login with Facebook     '}</Text>
            </Button>
          </View>
        </View>
        {
          isLoading ? (
            <Loading
              visible
              color="white"
              indicatorSize="large"
              messageFontSize={24}
              message=""
            />
          ) : null
        }
        {
          loginForm
            ? <LoginForm changeForm={this.changeForm} submitLogin={this.submitLogin} />
            : <SignupForm changeForm={this.changeForm} submitSignup={this.submitSignup} />
        }
      </KeyboardAvoidingView>
    );
  }
}

Login.propTypes = {
  loginSuccess: PropTypes.func,
};


export default Login;
