import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { Button, Input, Item } from 'native-base';
import Icon from 'react-native-vector-icons/EvilIcons';
// KeyboardAvoidingView
const styles = StyleSheet.create({
  buttonLogin: {
    backgroundColor: '#2abe87',
    height: 50,
    marginTop: 20,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 50,
  },
  buttonText: {
    textAlign: 'center',
    color: 'white',
    fontSize: 18,
  },
  itemInput: {
    marginLeft: 20,
    marginRight: 20,
  },
  input: {
    height: 40,
    marginTop: 10,
    marginBottom: 10,
    paddingHorizontal: 10,
    backgroundColor: 'rgba(255,255,255, 0.3)',
  },
  container: {
    padding: 20,
  },
});

export default class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Item underline style={styles.itemInput}>
          <Icon name="user" size={30} />
          <Input
            placeholder="Username or email"
            returnKeyType="next"
            style={styles.input}
            keyboardType="email-address"
            autoCapitalize="none"
            autoCorrect={false}
            onSubmitEditing={() => this.passwordInput._root.focus()}
            value={this.state.email}
            onChangeText={email => this.setState({ email })}
          />
        </Item>
        <Item underline style={styles.itemInput}>
          <Icon name="unlock" size={30} />
          <Input
            secureTextEntry
            style={styles.input}
            placeholder="Mật khẩu"
            value={this.state.password}
            autoCapitalize="none"
            ref={(input) => { this.passwordInput = input; }}
            onChangeText={password => this.setState({ password })}
          />
        </Item>
        <Button onPress={() => this.props.submitLogin(this.state)}
          block
          style={styles.buttonLogin}
        >
          <Text style={styles.buttonText}> Đăng nhập </Text>
        </Button>
        <Button onPress={this.props.changeForm} transparent block primary>
          <Text style={{ color: '#0000cd', fontSize: 18 }}> Đăng ký </Text>
        </Button>
      </View>
    );
  }
}
