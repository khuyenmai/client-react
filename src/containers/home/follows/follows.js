import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Dimensions, ListView, TouchableOpacity } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Text,
  Body,
  Right,
  Icon,
  Badge,
  Button,
  Spinner,
  Content,
  ListItem,
  Thumbnail,
  Container,
} from 'native-base';

import { actionGetCampaigns } from '../../../actions/followTabActions';
import { merchants, defaultLogo } from '../../../settings/list_campaigns.json';
import { followCampaign } from '../../../api/campaigns';

import HeaderBar from '../../../components/Header/Header';

const screenHeight = Dimensions.get('window').height;
const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
class Follows extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pageScroll: 0,
      following: props.following,
    };
  }

  componentDidMount() {
    this.props.getCampaigns();
  }


  onClickFollow = (campaign, isFollowing) => {
    followCampaign(campaign.merchant, isFollowing);

    const { following } = this.state;
    const { campaigns } = this.props;
    const follow = campaigns.find(({ _id }) => _id === campaign._id);
    if (!isFollowing && follow && !following.find(({ _id }) => _id === campaign._id)) {
      following.push(follow);
    } else if (isFollowing && follow) {
      _.remove(following, ({ _id }) => _id === campaign._id)
    }
    this.setState({ following });
  }

  getMerchant = (campaign) => {
    const campaignMerchant = (merchants || []).find((strMerchant) => {
      let name = campaign.merchant.split('.')[0].replace(/vn/g, '');
      if (name === 'vietravel') name = 'travel';
      return strMerchant.includes(name);
    });
    return campaignMerchant ? JSON.parse(campaignMerchant) : {};
  }

  loadMoreCampaigns = () => {
    let { pageScroll } = this.state;
    const { campaigns = [] } = this.props;
    if (pageScroll * 20 > campaigns.length) return;
    this.setState({
      pageScroll: (pageScroll += 1),
    });
  }

  render() {
    const { campaigns } = this.props;
    const { following } = this.state;
    const followingMerchants = following.map(({ merchant }) => merchant);
    return (
      <Container
        style={{
          height: screenHeight - 120,
        }}
      >
        <HeaderBar />
        <Content>
          {
            campaigns.length === 0 ? (
              <Container>
                <Spinner />
              </Container>
            )
              : (
                <Content>
                  <ListView
                    style={{ paddingLeft: 3 }}
                    onEndReached={this.loadMoreCampaigns}
                    onEndReachedThreshold={60}
                    dataSource={ds.cloneWithRows(campaigns || [])}
                    renderRow={rowData => (
                      <ListItem style={{ marginLeft: 0 }}>
                        <Thumbnail source={{ uri: this.getMerchant(rowData).website_logo || defaultLogo }} />
                        <Body>
                          <Text style={{ fontSize: 13 }}>
                            {rowData.merchant.split('.')[0].replace(/\b\w/g, l => l.toUpperCase())}
                          </Text>
                          <Text note style={{ fontSize: 12 }}>
                            {rowData.name}
                          </Text>
                        </Body>
                        <Right>
                          {
                            followingMerchants.includes(rowData.merchant) ? (
                              <TouchableOpacity onPress={() => this.onClickFollow(rowData, true)} >
                                <Badge style={{ backgroundColor: '#2abe87' }}>
                                  <Icon name="md-star-outline" style={{ fontSize: 19, color: "#fff", lineHeight: 25 }}/>
                                </Badge>
                              </TouchableOpacity>
                            ) : (
                              <TouchableOpacity onPress={() => this.onClickFollow(rowData, false)}>
                                <Badge style={{ backgroundColor: '#f4f5fc', flexDirection: 'row', }}>
                                  <Icon name="md-star" style={{ fontSize: 19, color: "#2abe87", lineHeight: 25 }} />
                                  <Text style={{ color: "#2abe87", fontSize: 13, lineHeight: 30 }}> Theo dõi </Text>
                                </Badge>
                              </TouchableOpacity>
                            )
                          }
                        </Right>
                      </ListItem>
                    )
                    }
                  />
                </Content>
              )
          }
        </Content>
      </Container>
    );
  }
}

Follows.propTypes = {
  // dispatch: PropTypes.func,
  getCampaigns: PropTypes.func,
  campaigns: PropTypes.array,
  following: PropTypes.array,
};

const mapDispatchToProps = dispatch => ({
  getCampaigns: () => actionGetCampaigns(dispatch),
});

const mapStateToProps = ({ campaignsReducer }) => ({
  campaigns: campaignsReducer.campaigns || [],
  following: campaignsReducer.following || [],
});

export default connect(mapStateToProps, mapDispatchToProps)(Follows);
