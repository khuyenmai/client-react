import React, { Component } from 'react';
import {
  StyleSheet, TouchableOpacity, Dimensions, AsyncStorage, Alert,
} from 'react-native';
import {
  Container, Header, Left, Body, Right, Content, Spinner, View,
} from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Login from '../../auth/login/login';
import SettingsScreen from './SettingsScreen';

import {
  actionGetOffers,
  actionGetTopProducts,
} from '../../../actions/salesTabActions';

import {
  actionGetFollowing,
} from '../../../actions/followTabActions';


const screenHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
  tabHeading: {
    backgroundColor: '#FFF',
  },
  headerLeft: {
    flex: 1 / 2,
  },
  headerRight: {
    flex: 1 / 2,
  },
  headerBody: {
    flex: 1,
    alignItems: 'center',
  },
  header: {
    backgroundColor: '#2abe87',
  },
});

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogin: null,
    };
    this.logOut = this.logOut.bind(this);
  }

  componentDidMount() {
    this.checkLogin();
  }

  checkLogin = async () => {
    try {
      const value = await AsyncStorage.getItem('user');
      this.setState({ isLogin: !!value });
    } catch (error) {
      console.log('Error', error);
    }
  }

  loginSuccess = () => {
    this.setState({ isLogin: true });
    const { dispatch } = this.props;
    actionGetFollowing(dispatch)
      .then(() => actionGetOffers(dispatch))
      .then(() => actionGetTopProducts(dispatch));
  }

  logOut() {
    const { dispatch } = this.props;
    Alert.alert('', 'Bạn muốn đăng xuất', [
      {
        text: 'Đăng xuất',
        onPress: async () => {
          try {
            await AsyncStorage.removeItem('user');
            this.setState({ isLogin: false });
            actionGetFollowing(dispatch);
          } catch (e) {
            actionGetFollowing(dispatch);
          }
        },
      },
      { text: 'Hủy', onPress: () => {} },
    ]);
  }

  renderContent = () => {
    const { navigation } = this.props;
    const { isLogin } = this.state;
    if (isLogin === null) {
      return (
        <Container>
          <Spinner />
        </Container>
      );
    } if (isLogin === false) {
      return (
        <Container>
          <Login loginSuccess={this.loginSuccess} navigation={navigation} />
        </Container>
      );
    }
    return (
      <Content>
        <SettingsScreen />
      </Content>
    );
  }

  render() {
    const { isLogin } = this.state;
    return (
      <Container
        style={{
          height: screenHeight - 120,
        }}
      >
        <Header style={styles.header}>
          <Left style={styles.headerLeft}>
            <TouchableOpacity>
              <Ionicons name="ios-options-outline" style={{ fontSize: 30, color: '#FFF' }} />
            </TouchableOpacity>
          </Left>
          <Body style={styles.headerBody}>
            {/* <Title> Đăng ký </Title> */}
          </Body>
          {
            isLogin ? (
              <Right style={styles.headerRight}>
                <TouchableOpacity onPress={this.logOut}>
                  <Ionicons name="ios-log-out" style={{ fontSize: 30, color: '#FFF' }} />
                </TouchableOpacity>
              </Right>
            ) : <View />
          }
        </Header>
        { this.renderContent() }
      </Container>
    );
  }
}

Profile.propTypes = {
  navigation: PropTypes.any,
  dispatch: PropTypes.any,
};

export default connect()(Profile);
