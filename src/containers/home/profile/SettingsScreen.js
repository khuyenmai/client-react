
import React, { Component } from 'react';
import {
  ScrollView, Switch, StyleSheet, Text, View,
} from 'react-native';
import {
  Thumbnail, ListItem, Content, List, Body, Left, Right, Icon,
} from 'native-base';
import PropTypes from 'prop-types';

import InfoText from './InfoText';
import user from './contact.json';

const styles = StyleSheet.create({
  scroll: {
    backgroundColor: 'white',
  },
  userRow: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingBottom: 8,
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 6,
  },
  userImage: {
    marginRight: 12,
  },
  listItemContainer: {
    height: 55,
    borderWidth: 0.5,
    borderColor: '#ECECEC',
  },
});

class SettingsScreen extends Component {
  static propTypes = {
    avatar: PropTypes.string,
    name: PropTypes.string,
    navigation: PropTypes.object,
    emails: PropTypes.arrayOf(
      PropTypes.shape({
        email: PropTypes.string,
      }),
    ),
  }

  state = {
    pushNotifications: true,
  }

  onPressOptions = () => {
    this.props.navigation.navigate('options');
  }

  onChangePushNotifications = () => {
    this.setState(state => ({
      pushNotifications: !state.pushNotifications,
    }));
  }

  render() {
    const { avatar, name, emails: [firstEmail] } = user;
    return (
      <ScrollView style={styles.scroll}>
        <View style={styles.userRow}>
          <View style={styles.userImage}>
            <Thumbnail
              source={{
                uri: avatar,
              }}
            />
          </View>
          <View>
            <Text style={{ fontSize: 16 }}>
              {name}
            </Text>
            <Text
              style={{
                color: 'gray',
                fontSize: 16,
              }}
            >
              {firstEmail.email}
            </Text>
          </View>
        </View>
        <InfoText text="Account" />
        <Content>
          <List>
            <ListItem icon>
              <Left>
                <Icon name="plane" />
              </Left>
              <Body>
                <Text>
Notifications
                </Text>
              </Body>
              <Right>
                <Switch value={false} />
              </Right>
            </ListItem>
            <ListItem icon>
              <Left>
                <Icon name="wifi" />
              </Left>
              <Body>
                <Text>
Wi-Fi
                </Text>
              </Body>
              <Right>
                <Text>
GeekyAnts
                </Text>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
            <ListItem icon>
              <Left>
                <Icon name="bluetooth" />
              </Left>
              <Body>
                <Text>
Bluetooth
                </Text>
              </Body>
              <Right>
                <Text>
On
                </Text>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
          </List>
        </Content>
      </ScrollView>
    );
  }
}

export default SettingsScreen;
