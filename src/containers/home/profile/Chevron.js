import React from 'react';
import { Icon } from 'native-base';

const Colors = {
  blue: '#008EFE',
};

const Chevron = () => (
  <Icon
    name="chevron-right"
    type="entypo"
    color={Colors.lightGray2}
    containerStyle={{ marginLeft: -15, width: 20 }}
  />
);

export default Chevron;
