import React, { Component } from 'react';
import {
  ScrollView, View, Text, TouchableOpacity,
} from 'react-native';
import { Item, Icon, Input } from 'native-base';
import { Row, Grid } from 'react-native-easy-grid';
import { connect } from 'react-redux';

import ProductCard from '../../../components/Product/ProductCard';
import Loading from '../../../components/Loading/Loading';
import { actionSearchProducts } from '../../../actions/salesTabActions';

class ProductsTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: '',
      isLoading: false,
    };
  }

  handleSearch = () => {
    this.setState({ isLoading: true });
    this.props.searchProducts(this.state.searchText)
      .then(() => this.setState({ isLoading: false }));
  }

  openDetail = (product) => {
    this.props.navigation.navigate('ProductDetail', product);
  }

  renderRow = () => {
    // console.log('renderRow')
    const products = this.props.products.slice();
    if (!products || products.length === 0) {
      return <View />;
    }
    // const rowNumber = products.length / 2;
    const rows = [];
    while (products.length > 0) {
      const productsRow = products.splice(0, 2);
      rows.push(
        <Row key={products.length} style={{ height: 380 }}>
          { productsRow.map(product => <ProductCard key={product._id} product={product} openDetail={this.openDetail} />)}
        </Row>,
      );
    }
    return rows;
  }

  render() {
    const { isLoading } = this.state;
    return (
      <ScrollView>
        <View
          style={{
            height: 50,
            flexDirection: 'row',
          }}
        >
          <Item style={{ flex: 4 }}>
            <Icon name="ios-search" />
            <Input placeholder="Search" onChangeText={searchText => this.setState({ searchText })} />
          </Item>
          <TouchableOpacity
            transparent
            style={{
              flex: 1, height: 50, justifyContent: 'center', alignItems: 'center',
            }}
            onPress={this.handleSearch}
          >
            <Text>
Search
            </Text>
          </TouchableOpacity>
        </View>
        {
          isLoading ? (
            <Loading
              visible
              color="white"
              indicatorSize="large"
              messageFontSize={24}
              message=""
            />
          ) : null
        }
        <Grid>
          { this.renderRow() }
        </Grid>
      </ScrollView>
    );
  }
}

const mapStateToProps = ({ homeReducer }) => ({
  products: homeReducer.products,
  navigation: homeReducer.currentnavigation,
});


const mapActionToProps = dispatch => ({
  searchProducts: searchText => dispatch(actionSearchProducts(searchText)),
});

export default connect(mapStateToProps, mapActionToProps)(ProductsTab);
