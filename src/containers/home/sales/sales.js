import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Container } from 'native-base';
import ScrollableTabView from 'react-native-scrollable-tab-view';

import OffersTab from './OffersTab';
import ProductsTab from './ProductsTab';

class Sales extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Container>
        <ScrollableTabView initialPage={0}>
          <OffersTab key="offer-tab" tabLabel="Khuyến mãi" />
          <ProductsTab key="product-tab" tabLabel="Sản Phẩm" />
        </ScrollableTabView>
      </Container>
    );
  }
}
const mapStateToProps = () => ({});

export default connect(mapStateToProps)(Sales);
