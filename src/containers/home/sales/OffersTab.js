import React, { Component } from 'react';
import {
  StyleSheet, ScrollView, Dimensions, View, Image, TouchableOpacity,
} from 'react-native';
import { List, Row } from 'native-base';
import PropTypes from 'prop-types';
import Carousel from 'react-native-carousel';

import { connect } from 'react-redux';
import OfferCard from '../../../components/Offer/OfferCard';
import { actionLike } from '../../../actions/salesTabActions';

const screenWidth = Dimensions.get('window').width;
const styles = StyleSheet.create({
  sliderOffer: {
    minHeight: 130,
    width: '100%',
  },
  filter: {
    height: 60,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  categoryIconView: {
    height: 60,
    width: 60,
    borderRadius: 50,
  },
  iconImg: {
    marginTop: 5,
    width: 50,
    height: 50,
  },
});

// const categories = [
//   { code: "E-COMMERCE", icon: "../../../../icons/online-shop.png" },
//   { code: "ONLINE SERVICES", icon: "../../../../icons/headset.png" },
//   { code: "FINANCIAL SERVICES", icon: "../../../../icons/wallet.png" },
//   { code: "TRAVEL and LEISURE", icon: "../../../../icons/paper-plane.png" },
//   { code: "EDUCATION", icon: "../../../../icons/mortarboard.png" },
//   // { code: "ENTERTAINMENT" },
// ];

class OffersTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageSize: {},
      sliderImages: props.offers.splice(0, 6),
    };
  }

  componentDidMount() {
    const { sliderImages } = this.state;
    sliderImages.forEach((offer) => {
      this.scaleImage(offer.image, offer.aid);
    });
  }

  handlePress = (offer) => {
    this.props.navigation.navigate('OfferDetail', offer);
  };

  scaleImage = (image, aid) => {
    Image.getSize(image, (width, height) => {
      const ratio = screenWidth / width;
      let imageHeight = (Math.round((height * ratio) / 10) * 10) + 8;
      if (imageHeight > 198) imageHeight = 307;
      this.setState({
        imageSize: {
          ...this.state.imageSize,
          [aid]: {
            width: +screenWidth.toFixed(0),
            height: imageHeight,
          },
        },
      });
    });
  }

  render() {
    const { offers = [], following } = this.props;
    const { sliderImages, imageSize } = this.state;
    const merchants = following.map(({ merchant }) => merchant);
    // console.log("following", following);
    const sliderCops = sliderImages.map(offer => (
      <View key={`image-${offer.aid}`}>
        <TouchableOpacity
          onPress={() => this.handlePress(offer)}
          style={{ height: (imageSize[offer.aid] && imageSize[offer.aid].height) || 130, width: '100%' }}
        >
          <Image
            source={{ uri: offer.image || '' }}
            style={{ height: (imageSize[offer.aid] && imageSize[offer.aid].height) || 130, width: '100%' }}
          />
        </TouchableOpacity>
      </View>
    ));
    return (
      <ScrollView>
        <View style={styles.sliderOffer}>
          <Carousel width="100%" delay={4000}>
            { sliderCops }
          </Carousel>
        </View>
        <List
          dataArray={offers}
          renderRow={item => (
            <Row>
              <OfferCard
                offer={{ ...item }}
                actionLike={actionLike}
                followingMerchants={merchants}
              />
            </Row>
          )}
        />
      </ScrollView>
    );
  }
}

OffersTab.propTypes = {
  offers: PropTypes.array,
  navigation: PropTypes.object,
  following: PropTypes.array,
};

const mapStateToProps = ({ homeReducer, campaignsReducer }) => ({
  offers: homeReducer.offers,
  navigation: homeReducer.currentnavigation,
  following: campaignsReducer.following,
});

export default connect(mapStateToProps)(OffersTab);
