import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Container,
  FooterTab,
  Drawer,
  Footer,
  Button,
  Text,
  Tabs,
  Tab,
} from 'native-base';

import Ionicons from 'react-native-vector-icons/Ionicons';

import Intro from '../intro';
import Sales from './sales/sales';
import Follows from './follows/follows';
import Profile from './profile/profile';
import Menu from '../../components/menu';
import Notifications from './notifications/notifications';

import {
  actionGetOffers,
  actionGetTopProducts,
  updateCurrentNavigator,
} from '../../actions/salesTabActions';

import {
  actionGetFollowing,
} from '../../actions/followTabActions';

const tabsName = ['sales', 'follows', 'notify', 'profile'];
const styles = StyleSheet.create({
  header: {
    backgroundColor: '#2abe87',
  },
  footertab: {
    backgroundColor: '#FFF',
  },
  footer: {
    backgroundColor: '#FFF',
  },
  iconTab: {
    color: '#999',
    fontSize: 30,
  },
  iconText: {
    color: '#999',
    fontSize: 9,
  },
  buttonTab: {
    paddingTop: 10,
  },
  contentContainer: {
    backgroundColor: 'white',
  },
  active: {
    color: '#69C9A5',
  },
});
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = { active: 'sales', openMenu: false };
    this.changeFooterTab = this.changeFooterTab.bind(this);
  }

  componentDidMount() {
    const { dispatch, navigation } = this.props;
    actionGetFollowing(dispatch)
      .then(() => actionGetOffers(dispatch))
      .then(() => actionGetTopProducts(dispatch))
      .then(() => updateCurrentNavigator(dispatch, navigation));
  }

  changeFooterTab(i) {
    if (this.state.active === tabsName[i]) return;
    this.setState({
      active: tabsName[i],
    });
  }

  closeDrawer = () => {
    this.drawer._root.close();
  };

  openDrawer = () => {
    this.drawer._root.open();
  };

  render() {
    const { fetchingDone, navigation } = this.props;

    if (!fetchingDone) {
      return <Intro />;
    }
    return (
      <Drawer
        ref={(ref) => { this.drawer = ref; }}
        content={<Menu navigator={this.navigator} />}
        onClose={() => this.closeDrawer()}
      >
        <Container>
          <Tabs
            locked
            initialPage={0}
            ref={(tabview) => { this.tabview = tabview; }}
            onChangeTab={({ i }) => this.changeFooterTab(i)}
            renderTabBar={() => <View style={{ display: 'none' }} />}
          >
            <Tab heading="">
              <Sales />
            </Tab>
            <Tab heading="">
              <Follows />
            </Tab>
            <Tab heading="">
              <Notifications />
            </Tab>
            <Tab heading="">
              <Profile navigation={navigation} />
            </Tab>
          </Tabs>
          <Footer style={styles.footer}>
            <FooterTab style={styles.footertab}>
              <Button
                style={[styles.buttonTab]}
                vertical
                onPress={() => {
                  this.tabview.goToPage(0);
                }}
              >
                <Ionicons
                  style={[styles.iconTab, this.state.active === tabsName[0] && styles.active]}
                  name="ios-ribbon-outline"
                />
                <Text style={[styles.iconText, this.state.active === tabsName[0] && styles.active]}>
                  Khuyến mãi
                </Text>
              </Button>
              <Button
                style={[styles.buttonTab]}
                vertical
                onPress={() => {
                  this.tabview.goToPage(1);
                }}
              >
                <Ionicons
                  style={[styles.iconTab, this.state.active === tabsName[1] && styles.active]}
                  name="md-star-outline"
                />
                <Text style={[styles.iconText, this.state.active === tabsName[1] && styles.active]}>
                  Theo dõi
                </Text>
              </Button>
              <Button
                style={[styles.buttonTab]}
                onPress={() => {
                  this.tabview.goToPage(2);
                }}
                vertical
              >
                <Ionicons
                  style={[styles.iconTab, this.state.active === tabsName[2] && styles.active]}
                  name="ios-notifications-outline"
                />
                <Text style={[styles.iconText, this.state.active === tabsName[2] && styles.active]}>
                  Thông báo
                </Text>
              </Button>
              <Button
                style={[styles.buttonTab]}
                vertical
                onPress={() => {
                  this.tabview.goToPage(3);
                }}
              >
                <Ionicons
                  style={[styles.iconTab, this.state.active === tabsName[3] && styles.active]}
                  name="md-contact"
                />
                <Text style={[styles.iconText, this.state.active === tabsName[3] && styles.active]}>
                  Cá nhân
                </Text>
              </Button>
            </FooterTab>
          </Footer>
        </Container>
      </Drawer>
    );
  }
}

Home.propTypes = {
  // openMenu: PropTypes.bool,
  dispatch: PropTypes.func,
  fetchingDone: PropTypes.bool,
  navigation: PropTypes.object,
};

const mapStateToProps = ({ homeReducer }) => ({
  openMenu: homeReducer.openMenu || false,
  fetchingDone: homeReducer.fetchingDone || false,
  currentNavigator: homeReducer.currentNavigator || {},
});

export default connect(mapStateToProps)(Home);
