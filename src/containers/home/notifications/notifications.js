import React, { Component } from 'react';
import {
  StyleSheet, TouchableOpacity, Dimensions, ListView,
} from 'react-native';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Content,
  ListItem,
  Text,
} from 'native-base';

import Ionicons from 'react-native-vector-icons/Ionicons';

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

const screenHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
  tabHeading: {
    backgroundColor: '#FFF',
  },
  headerLeft: {
    flex: 1 / 2,
  },
  headerRight: {
    flex: 1 / 2,
  },
  headerBody: {
    flex: 1,
    alignItems: 'center',
  },
  header: {
    backgroundColor: '#2abe87',
  },
});

export default class Notifications extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const notifications = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];
    return (
      <Container
        style={{
          height: screenHeight - 120,
        }}
      >
        <Header style={styles.header}>
          <Left style={styles.headerLeft}>
            <TouchableOpacity>
              <Ionicons name="ios-options-outline" style={{ fontSize: 30, color: '#FFF' }} />
            </TouchableOpacity>
          </Left>
          <Body style={styles.headerBody}>
            {/* <Title> Đăng ký </Title> */}
          </Body>
          <Right style={styles.headerRight}>
            <TouchableOpacity>
              <Ionicons name="ios-search-outline" style={{ fontSize: 30, color: '#FFF' }} />
            </TouchableOpacity>
          </Right>
        </Header>
        <Content>
          <ListView
            style={{ paddingLeft: 3 }}
            onEndReached={this.loadMoreCampaigns}
            onEndReachedThreshold={60}
            dataSource={ds.cloneWithRows(notifications || [])}
            renderRow={rowData => (
              <ListItem style={{ marginLeft: 0 }}>
                <Body>
                  <Text style={{ fontSize: 13 }}>
                    {' '}
notifications
                    {' '}
                  </Text>
                  <Text note style={{ fontSize: 12 }}>
note
                  </Text>
                </Body>
              </ListItem>
            )}
          />
        </Content>
      </Container>
    );
  }
}
