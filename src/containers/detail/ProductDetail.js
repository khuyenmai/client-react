import React, { Component } from 'react';
import {
  Image, View, Text, TouchableOpacity, Dimensions, Linking,
} from 'react-native';
import PropTypes from 'prop-types';
import {
  Container,
  CardItem,
  Header,
  Content,
  Card,
  Left,
  Thumbnail,
  Body,
  Button,
  Icon,
  Right,
  Tabs,
  Tab,
  TabHeading,
} from 'native-base';
import moment from 'moment';

const screenWidth = Dimensions.get('window').width;

function capitalizeString(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

class ProductDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageSize: {
        height: 0,
      },
    };
  }

  componentDidMount() {
    const product = this.props.navigation.state.params;
    const uri = this.getImageUrl(product);
    this.scaleImage(uri);
  }

  getImageUrl(product) {
    return product.image;
  }

  scaleImage = (image) => {
    Image.getSize(image, (width, height) => {
      const ratio = screenWidth / width;
      const imageHeight = Math.round((height * ratio) / 10) * 10 + 8;
      // if (imageHeight > 198) imageHeight = 307;

      this.setState({
        imageSize: {
          height: imageHeight,
        },
      });
    });
  }

  render() {
    const { goBack } = this.props.navigation;
    const product = this.props.navigation.state.params;
    const { imageSize } = this.state;
    return (
      <Container>
        <Header
          style={{
            backgroundColor: '#2abe87',
          }}
        >
          <Left>
            <TouchableOpacity onPress={() => goBack(null)}>
              <Icon name="md-arrow-back" style={{ fontSize: 30, color: '#FFF' }} />
            </TouchableOpacity>
          </Left>
          <Body />
          <Right>
            <TouchableOpacity>
              <Icon name="md-heart-outline" style={{ fontSize: 30, color: 'pink' }} />
            </TouchableOpacity>
          </Right>
        </Header>
        <Content>
          <Card>
            <CardItem>
              <Left>
                <Thumbnail
                  source={product.logo ? { uri: product.logo } : require('./../../images/tag.png')}
                  style={{ height: 32, width: 32 }}
                />
                <Body>
                  <Text>
                    {capitalizeString(product.brand || product.campaign || '')}
                  </Text>
                  <Text style={{ fontSize: 12 }}>
                    { `${String(product.price).replace(/\B(?=(\d{3})+(?!\d))/g, '.')}đ` }
                  </Text>
                </Body>
              </Left>
              <Right style={{ width: 100 }}>
                <Button transparent success onPress={() => { Linking.openURL(product.aff_link || ''); }}>
                  <Text style={{ color: '#0645AD' }}>
                    {' '}
Trang chủ
                    {' '}
                  </Text>
                </Button>
              </Right>
            </CardItem>
            <CardItem>
              <Text
                style={{
                  fontSize: 17,
                  marginLeft: 10,
                  marginRight: 10,
                  fontWeight: '700',
                  color: '#2abe87',
                }}
              >
                {product.name}
              </Text>
            </CardItem>
            <CardItem cardBody>
              <TouchableOpacity
                style={{ flex: 1, width: '100%', height: imageSize.height }}
                onPress={() => { Linking.openURL(product.aff_link || ''); }}
              >
                <Image
                  source={{ uri: this.getImageUrl(product) }}
                  style={{ width: '100%', height: imageSize.height }}
                />
              </TouchableOpacity>
            </CardItem>
            <CardItem>
              <View style={{ flex: 1 }}>
                <TouchableOpacity
                  transparent
                  onPress={this.handleLike}
                >
                  <Icon style={{ fontSize: 16 }} name="ios-thumbs-up">
                    <Text>
                      {' '}
                      {product.like || 0}
                    </Text>
                  </Icon>
                </TouchableOpacity>
              </View>
              <View style={{ flex: 1 }}>
                <TouchableOpacity>
                  <Icon style={{ fontSize: 16 }} name="ios-chatbubbles-outline">
                    <Text>
                      {' '}
4
                    </Text>
                  </Icon>
                </TouchableOpacity>
              </View>
              <View style={{ flex: 1 }}>
                <Icon style={{ fontSize: 16, width: 150 }} name="ios-timer-outline">
                  <Text>
                    {' '}
                    {moment(product.end_time).format('DD/MM/YYYY')}
                  </Text>
                </Icon>
              </View>
            </CardItem>
            <CardItem
              cardBody
              style={{
                height: 140,
              }}
            >
              <Content>
                <Tabs>
                  <Tab
                    heading={(
                      <TabHeading style={{ backgroundColor: '#FFF' }}>
                        <Text style={{ color: 'grey' }}>
Thông tin
                        </Text>
                      </TabHeading>
)}
                  >
                    <Body>
                      <Text>
                        {product.content}
                      </Text>
                    </Body>
                  </Tab>
                  <Tab
                    heading={(
                      <TabHeading style={{ backgroundColor: '#FFF' }}>
                        <Text style={{ color: 'grey' }}>
Thương hiệu
                        </Text>
                      </TabHeading>
)}
                  >
                    <View>
                      <Text>
Thương Hiệu
                      </Text>
                    </View>
                  </Tab>
                </Tabs>
              </Content>
            </CardItem>
            <CardItem>
              <Left>
                <Text>
Bình luận(19)
                </Text>
              </Left>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}

ProductDetail.propTypes = {
  product: PropTypes.object,
  navigation: PropTypes.object,
};

export default ProductDetail;
