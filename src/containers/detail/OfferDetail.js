import React, { Component } from 'react';
import {
  Image, View, Text, TouchableOpacity, Dimensions, Linking,
} from 'react-native';
import PropTypes from 'prop-types';
import {
  Container,
  CardItem,
  Header,
  Content,
  Card,
  Left,
  Thumbnail,
  Body,
  Button,
  Icon,
  Right,
  Tabs,
  Tab,
  TabHeading,
} from 'native-base';
import moment from 'moment';
import _ from 'lodash';

const screenWidth = Dimensions.get('window').width;

const capitalizeString = string => string.charAt(0).toUpperCase() + string.slice(1);

class OfferDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageSize: {
        height: 0,
      },
    };
  }

  componentDidMount() {
    const offer = this.props.navigation.state.params;
    const uri = this.getImageUrl(offer);
    this.scaleImage(uri);
  }

  getImageUrl(offer) {
    const ban = _.find(
      offer.banners,
      banner => banner.width === 600 && banner.height === 600,
    );
    return ban ? ban.link : offer.image;
  }

  scaleImage = (image) => {
    Image.getSize(image, (width, height) => {
      const ratio = screenWidth / width;
      const imageHeight = Math.round((height * ratio));
      this.setState({
        imageSize: {
          width: +screenWidth.toFixed(0),
          height: imageHeight,
        },
      });
    });
  }

  render() {
    const { goBack } = this.props.navigation;
    const offer = this.props.navigation.state.params;
    const { imageSize } = this.state;

    return (
      <Container>
        <Header
          style={{
            backgroundColor: '#2abe87',
          }}
        >
          <Left>
            <TouchableOpacity onPress={() => goBack(null)}>
              <Icon name="md-arrow-back" style={{ fontSize: 30, color: '#FFF' }} />
            </TouchableOpacity>
          </Left>
          <Body />
          <Right>
            <TouchableOpacity>
              <Icon name="md-heart-outline" style={{ fontSize: 30, color: 'pink' }} />
            </TouchableOpacity>
          </Right>
        </Header>
        <Content>
          <Card style={{ flex: 1 }}>
            <CardItem>
              <Left>
                <Thumbnail
                  source={{ uri: offer.logo }}
                  style={{ height: 40, width: 40 }}
                />
                <Body>
                  <Text>
                    {capitalizeString(offer.merchant.split('.')[0])}
                  </Text>
                  <Text note>
                    { moment(offer.end_time)
                      .locale('vi')
                      .format('ll')}
                  </Text>
                </Body>
              </Left>
              <Right style={{ width: 100 }}>
                <Button transparent success onPress={() => { Linking.openURL(offer.aff_link || ''); }}>
                  <Text style={{ color: '#0645AD' }}>
                    {' '}
Trang chủ
                    {' '}
                  </Text>
                </Button>
              </Right>
            </CardItem>
            <CardItem cardBody>
              <TouchableOpacity
                onPress={() => { Linking.openURL(offer.aff_link || ''); }}
                style={{ width: '100%' }}
              >
                <Image
                  source={{ uri: this.getImageUrl(offer) }}
                  style={{ flex: 1, width: '100%', height: imageSize.height }}
                />
                <Text
                  style={{
                    fontSize: 17,
                    fontWeight: '700',
                    marginLeft: 10,
                    marginRight: 10,
                    marginTop: 10,
                    color: '#2abe87',
                  }}
                >
                  {offer.name}
                </Text>
              </TouchableOpacity>
            </CardItem>
            <CardItem>
              <View style={{ flex: 1 }}>
                <TouchableOpacity
                  transparent
                  onPress={this.handleLike}
                >
                  <Icon style={{ fontSize: 16 }} name="ios-thumbs-up">
                    <Text>
                      {' '}
                      {offer.like || 0}
                    </Text>
                  </Icon>
                </TouchableOpacity>
              </View>
              <View style={{ flex: 1 }}>
                <TouchableOpacity>
                  <Icon style={{ fontSize: 16 }} name="ios-chatbubbles-outline">
                    <Text>
                      {' '}
4
                    </Text>
                  </Icon>
                </TouchableOpacity>
              </View>
              <View style={{ flex: 1 }}>
                <Icon style={{ fontSize: 16, width: 150 }} name="ios-timer-outline">
                  <Text>
                    {' '}
                    {moment(offer.end_time).format('DD/MM/YYYY')}
                  </Text>
                </Icon>
              </View>
            </CardItem>
            <CardItem cardBody>
              <Tabs>
                <Tab
                  heading={(
                    <TabHeading style={{ backgroundColor: '#FFF' }}>
                      <Text style={{ color: 'grey' }}>
Thông tin
                      </Text>
                    </TabHeading>
)}
                >
                  <Body style={{ paddingLeft: 20, paddingRight: 20, paddingTop: 30 }}>
                    <Text>
                      {offer.content}
                    </Text>
                  </Body>
                </Tab>
                <Tab
                  heading={(
                    <TabHeading style={{ backgroundColor: '#FFF' }}>
                      <Text style={{ color: 'grey' }}>
Thương hiệu
                      </Text>
                    </TabHeading>
)}
                >
                  <View style={{ paddingLeft: 20, paddingRight: 20, paddingTop: 30 }}>
                    <Text>
Thương Hiệu
                    </Text>
                  </View>
                </Tab>
              </Tabs>
            </CardItem>
            <CardItem>
              <Left>
                <Text>
Bình luận(19)
                </Text>
              </Left>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}

OfferDetail.propTypes = {
  offer: PropTypes.object,
  navigation: PropTypes.object,
};

export default OfferDetail;
