import { combineReducers } from 'redux';
import homeReducer from './home';
import campaignsReducer from './campaigns';

export default combineReducers({
  homeReducer,
  campaignsReducer,
});
