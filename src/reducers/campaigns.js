import { GET_CAMPAINS_DONE, GET_FOLLWING_DONE } from '../actions/constants';

const initialState = {
  campaigns: [],
  following: [],
};
export default function campaignsReducer(state = initialState, action) {
  switch (action.type) {
    case GET_CAMPAINS_DONE:
      return { ...state, campaigns: action.payload };
    case GET_FOLLWING_DONE:
      return { ...state, following: action.payload };
    default:
      return state;
  }
}
