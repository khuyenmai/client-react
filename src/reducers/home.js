import {
  FETCHING_DONE, ORDER_DETAIL, UPDATE_CURRENT_NAVIGATOR, TOGGLE_MENU,
} from '../actions/constants';

const initialState = {
  fetchingDone: false,
  offers: [],
  products: [],
  offerDetail: null,
  openMenu: false,
  currentnavigation: {},
};
export default function homeReducer(state = initialState, action) {
  const fetchingDone = (state.offers && action.products) || (state.products && action.offers);
  switch (action.type) {
    case FETCHING_DONE:
      return {
        ...state,
        fetchingDone: !!fetchingDone,
        offers: action.offers || state.offers,
        products: action.products || state.products,
      };
    case ORDER_DETAIL:
      return {
        ...state,
        offerDetail: action.offerDetail,
      };
    case UPDATE_CURRENT_NAVIGATOR:
      return {
        ...state,
        currentnavigation: action.currentnavigation,
      };
    case TOGGLE_MENU:
      return {
        ...state,
        openMenu: !state.openMenu,
      };
    default:
      return state;
  }
}
